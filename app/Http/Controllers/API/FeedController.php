<?php

namespace App\Http\Controllers\API;

use FeedIo\Factory as Feed;
use App\Http\Controllers\Controller;

class FeedController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {

    }

    /**
     * @return mixed
     */
    public function getRssSource()
    {
        return env('RSS_SOURCE');
    }

    /**
     * Get RSS feed data.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFeed()
    {
        return response()->json([
            'data' => $this->readFeed($this->getRssSource()),
        ]);
    }

    /**
     * Reed and return feed content
     *
     * @param string $url
     * @return array
     */
    public function readFeed(string $url)
    {
        $feedData = [];
        $reader = Feed::create()->getFeedIo();
        $feed = $reader->read($url)->getFeed();

        foreach($feed as $item) {
            $feedData[] = $this->extractFeedItemData($item);
        }

        return $feedData;
    }

    /**
     * Extract necessary data from feed
     *
     * @param $item
     * @return array
     */
    protected function extractFeedItemData($item)
    {
        $imageUrl = [];
        // Trick to get protected img url value
        foreach ($item->getMedias() as $image) {
            $imageUrl = (array)$image;
        }

        return [
            'title' => $item->getTitle(),
            'description' => $item->getDescription(),
            'link' => $item->getLink(),
            'img' => $imageUrl["\0*\0" . 'url'] ?? [],
            'date' => date_format($item->getLastModified(), 'd.m.Y H:i'),
        ];
    }
}
