<?php

namespace App\Http\Controllers\API\Settings;

use App\Http\Controllers\Controller;
use App\Http\Requests\Settings\UpdatePasswordRequest;
use Illuminate\Http\Request;

class PasswordController extends Controller
{
    /**
     * Update the user's password.
     * 
     * @param UpdatePasswordRequest $request
     *
     * @return mixed
     */
    public function update(UpdatePasswordRequest $request)
    {
        $user = $request->user();

        return tap($user)->update([
            'password' => bcrypt($request->password)
        ]);
    }
}
