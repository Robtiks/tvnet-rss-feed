<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use App\Http\Requests\Auth\LoginRequest;
use App\User;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function login(LoginRequest $request)
    {
        $user = User::whereEmail($request->only(['email']));

        if ($user->exists() && !$user->whereVerified(1)->exists()) {
            throw ValidationException::withMessages([
                'email' => [trans('auth.email_not_verified')],
            ]);
        }

        if ($token = Auth::attempt($request->only(['email', 'password']))) {
            return response()->json([
                'token' => $token
            ]);
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::logout();

        return response()->json([
            'success' => true
        ]);
    }
}
