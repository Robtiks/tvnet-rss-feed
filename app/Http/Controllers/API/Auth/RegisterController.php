<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Requests\Auth\sendEmailConfirm;
use App\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create new user
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $this->validator($request->all())->validate();
        $data = $request->all();
        event(new Registered($user = $this->createUser($data)));
        $user->sendConfirmation();

        return response()->json([
            'status' => 'success'
        ]);
    }

    /**
     * @param sendEmailConfirm $request
     *
     * @return string
     */
    public function confirmEmail(sendEmailConfirm $request)
    {
        $userConfirmed = User::whereEmail($request->only(['email']))
            ->whereEmailConfirmKey($request->only(['token']));

        if (!$userConfirmed->exists()) {
            return abort(404);
        }

        $user = $userConfirmed->first();
        $user->verified = 1;
        $user->save();

        // Bad response implementation, bet good enough for now
        return redirect()->route('login', ['emailConfirmed' => true]);
    }

    /**
     * Resend confirmation email to user.
     *
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendConfirmation($userId)
    {
        $user = User::whereNull('verified')->findOrFail($userId);
        $user->sendConfirmation();
    }

    /**
     * @param array $data
     *
     * @return mixed
     */
    public function createUser(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }
}
