<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as Notification;

class ResetPassword extends Notification
{
    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(trans('email.reset_password'))
            ->line(trans('email.reset_password_header'))
            ->action(trans('email.reset_password'), url(config('app.url').'/password/reset/'.$this->token).'?email='.urlencode($notifiable->email))
            ->line(trans('email.reset_password_after_info'));
    }
}
