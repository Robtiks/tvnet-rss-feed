<?php

namespace App;

use Hamcrest\Thingy;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\CanResetPassword as ResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\ConfirmEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class User extends Model implements AuthenticatableContract, AuthorizableContract, JWTSubject, ResetPasswordContract
{
    use Authenticatable,
        Authorizable,
        CanResetPassword,
        Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'verified', 'email_confirm_key'
    ];

    /**
     * Send confirmation email to user and (re)set his confirmation hash.
     * @return $this
     */
    public function sendConfirmation()
    {
        $this->email_confirm_key = sha1(rand());

        $this->sendEmailConfirmNotification($this->email_confirm_key);

        $this->save();

        return $this;
    }

    /**
     * Send the email confirmation notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendEmailConfirmNotification($token)
    {
        $this->notify(new ConfirmEmail($token, $this->email));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Get the oauth providers.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function oauthProviders()
    {
        return $this->hasMany(OAuthProvider::class);
    }

    /**
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
