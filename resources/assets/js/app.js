import Vue from 'vue'
import store from '~/store'
import router from '~/router'
import App from '~/components/App'
import i18n from '~/plugins/i18n'
import VueAnalytics from 'vue-analytics'
import VueLazyload from 'vue-lazyload'

import '~/plugins'
import '~/components'

Vue.config.productionTip = false

Vue.use(VueLazyload, {
  preLoad: 1.7778
})

Vue.use(VueAnalytics, {
  id: 'UA-134566670-1'
})

new Vue({
  i18n,
  store,
  router,
  ...App
})
