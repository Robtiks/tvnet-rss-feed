<?php

return [

    'reset_password_header' => 'Jūs saņemat šo e-pasta ziņojumu, jo saņēmām jūsu konta paroles atiestatīšanas pieprasījumu.',
    'reset_password' => 'Atiestatīt paroli',
    'reset_password_after_info' => 'Ja neesat pieprasījis paroles atiestatīšanu, turpmākas darbības nav jāveic.',

    'confirm_email_header' => 'Atcerieties apstiprināt savu e-pasta adresi',
    'confirm_email_text' => 'Neaizmirstiet apstiprināt savu e-pasta adresi, lai pabeigtu reģistrāciju.',
    'confirm_email' => 'Apstiprināt e-pastu',

];