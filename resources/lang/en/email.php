<?php

return [

    'reset_password_header' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_password' => 'Reset Password',
    'reset_password_after_info' => 'If you did not request a password reset, no further action is required.',

    'confirm_email_header' => 'Remember to confirm your email address',
    'confirm_email_text' => 'Don\'t forget to confirm your email address to complete your registration.',
    'confirm_email' => 'Confirm Email',

];