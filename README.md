# TVNET-RSS feed
TVNET RSS feed reader with LUMEN and VUE.JS

## Prerequisites
* PHP (>7.1)
* NodeJS (>8)

## Installation
- `git clone https://Robtiks@bitbucket.org/Robtiks/tvnet-rss-feed.git`
- `cd [repo]`
- Edit `.env` and set your database connection details
- `composer install`
- `php artisan migrate`
- `php artisan jwt:secret`
- `npm install`
- `npm run dev`
